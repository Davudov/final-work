//package com.example.diplomawork.utils
//
//import android.content.Context
//import android.util.AttributeSet
//import android.view.View
//import androidx.viewpager.widget.ViewPager
//import androidx.viewpager2.widget.ViewPager2
//
//
//class NestingViewPager : ViewPager2 {
//    constructor(context: Context?, attrs: AttributeSet?) : super(context!!, attrs) {}
//    constructor(context: Context?) : super(context!!) {}
//
//    override fun canScroll(
//        v: View,
//        checkV: Boolean,
//        dx: Int,
//        x: Int,
//        y: Int
//    ): Boolean {
//        return if (v !== this && v is ViewPager) {
//            true
//        } else super.canScroll(v, checkV, dx, x, y)
//    }
//}