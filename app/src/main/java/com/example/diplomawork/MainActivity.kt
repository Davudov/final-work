package com.example.diplomawork

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.core.view.GravityCompat
import com.arellomobile.mvp.MvpAppCompatActivity
import com.example.diplomawork.adapters.FragmentStateAdapter
import com.example.diplomawork.ui.compiler.CompilerActivity
import com.example.diplomawork.ui.description.DescriptionFragment
import com.example.diplomawork.ui.practise.PractiseFragment
import com.example.diplomawork.ui.tutorials.TutorialsFragment
import com.google.android.material.navigation.NavigationView
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : MvpAppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    lateinit var adapterActivityStateAdapter: FragmentStateAdapter
    private val tabName = arrayOf("Tutorials", "Practise", "Description")
    private val fragments: ArrayList<BaseFragment> =
        arrayListOf(
            TutorialsFragment(),
            PractiseFragment(),
            DescriptionFragment()
        )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.setBackgroundDrawableResource(R.drawable.top_panel_back)
        adapterActivityStateAdapter =
            FragmentStateAdapter(supportFragmentManager, lifecycle, fragments)
        view_pager_parent.adapter = adapterActivityStateAdapter
        TabLayoutMediator(tabLayout, view_pager_parent) { tab, position ->
            tab.text = tabName[position]
            view_pager_parent.setCurrentItem(tab.position, true)
        }.attach()
        navigation_view.setNavigationItemSelectedListener(this)
        navigation_icon.setOnClickListener {
                drawer_layout.openDrawer(
                    GravityCompat.START
                )
        }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_home -> drawer_layout.closeDrawer(GravityCompat.START)
            R.id.nav_compiler -> {
                startActivity(Intent(this, CompilerActivity::class.java))
            }
            R.id.nav_exit -> finish()
        }
        return false
    }

}
