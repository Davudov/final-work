package com.example.diplomawork.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.recyclerview.widget.RecyclerView
import com.example.diplomawork.R
import com.example.diplomawork.callback.NextListener
import com.example.diplomawork.entity.Exercise
import kotlinx.android.synthetic.main.item_quiz.view.*

class QuizAdapter(private val list: List<Exercise>, val nextListener: NextListener) :
    RecyclerView.Adapter<QuizAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(
        LayoutInflater.from(
            parent.context
        ).inflate(
            R.layout.item_quiz, parent, false
        ), nextListener, list
    )

    override fun getItemCount(): Int = list.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position])
    }

    class ViewHolder(
        itemView: View,
        private val nextListener: NextListener,
        private val list: List<Exercise>
    ) : RecyclerView.ViewHolder(itemView) {

        fun bind(exercise: Exercise) {
            itemView.text_view_question_count.text =
                "Question: ${absoluteAdapterPosition + 1}/${list.size}"
            itemView.question.text = exercise.exercise_question
            itemView.radioButton_a.text = exercise.choice_a
            itemView.radioButton_b.text = exercise.choice_b
            itemView.radioButton_c.text = exercise.choice_c
            itemView.radioButton_d.text = exercise.choice_d
            itemView.check_answer_btn.setOnClickListener {
                checkRadioButton(exercise)
            }
        }

        private fun checkRadioButton(exercise: Exercise) {
            val selectedId = itemView.radioGroup.checkedRadioButtonId
            if (selectedId != -1) {
                val selectedRadioButton = itemView.findViewById<RadioButton>(selectedId)
                if (itemView.radioGroup[exercise.answer-1].id == itemView.radioGroup.checkedRadioButtonId) {
                    itemView.explanation_text.text = exercise.explanation
                    if (absoluteAdapterPosition == list.size - 1) {
                        itemView.check_answer_btn.text = "Finish"
                        itemView.check_answer_btn.setOnClickListener(onFinishClick)
                    } else {
                        itemView.check_answer_btn.text = "Next"
                        itemView.check_answer_btn.setOnClickListener(onNextClick)
                    }

                    selectedRadioButton.setBackgroundColor(
                        ContextCompat.getColor(
                            itemView.context,
                            R.color.solve_btn_color
                        )
                    )
                } else {
                    itemView.check_answer_btn.text = "Check"
                    selectedRadioButton.setBackgroundColor(
                        ContextCompat.getColor(
                            itemView.context,
                            android.R.color.holo_red_dark
                        )
                    )

                }

            } else Toast.makeText(itemView.context, "Select option", Toast.LENGTH_LONG).show()
        }


        private val onNextClick = View.OnClickListener { v -> nextListener.onNext() }
        private val onFinishClick = View.OnClickListener { v -> nextListener.onFinish() }

    }
}