package com.example.diplomawork.ui.contents.contents

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.diplomawork.BaseFragment
import com.example.diplomawork.R
import com.example.diplomawork.adapters.ContentsAdapter
import com.example.diplomawork.di.APP
import com.example.diplomawork.entity.Content
import com.example.diplomawork.entity.ContentData
import com.example.diplomawork.listeners.OnClickListener
import com.example.diplomawork.ui.tutorials.quiz.QuizActivity
import com.example.diplomawork.utils.FadeAnimation
import com.example.diplomawork.utils.Screens
import kotlinx.android.synthetic.main.activity_contents.*
import ru.terrakok.cicerone.Router
import toothpick.ktp.KTP
import javax.inject.Inject

class ContentsFragment : BaseFragment(),
    OnClickListener {

    @Inject
    lateinit var router: Router

    @Inject
    lateinit var contentData: ContentData


    var contentList: ArrayList<Content> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        KTP.openScope(APP).inject(this)
        super.onCreate(savedInstanceState)
        val chapterId = activity?.intent?.getIntExtra("chapter_id", 999)
        contentList.addAll(contentData.content_list.filter { t ->
            t.chapter_id == chapterId
        }
        )
        contentList.add(contentList.size, Content(0, "", 0, "Quiz", ""))
    }

    override fun getLayoutId(): Int = R.layout.activity_contents

    override fun viewCreated(savedInstanceState: Bundle?) {
        val arr: Array<View> = arrayOf(
            contents_recycler
        )
        val animUtil = FadeAnimation(context!!)
        animUtil.addView(arr)
        contents_recycler.layoutManager = LinearLayoutManager(context)
        contents_recycler.adapter = ContentsAdapter(contentList, this)
        val contentsToolbar: androidx.appcompat.widget.Toolbar =
            activity!!.findViewById(R.id.contents_toolbar)
        contentsToolbar.title = activity?.intent?.getStringExtra("chapter_name")

    }

    override fun onClick(view: View, position: Int) {
        if (position == contentList.size - 1) {
            startActivity(
                QuizActivity.getInstance(
                    context,
                    activity?.intent?.getIntExtra("chapter_id", 999)!!
                )
            )
        } else
            router.navigateTo(
                Screens.ContentDetailsScreen(
                    contentList[position].content_id
                )
            )
    }
}