package com.example.diplomawork.ui.tutorials.quiz

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.viewpager2.widget.ViewPager2
import com.example.diplomawork.R
import com.example.diplomawork.adapters.QuizAdapter
import com.example.diplomawork.callback.NextListener
import com.example.diplomawork.di.APP
import com.example.diplomawork.entity.ExerciseData
import kotlinx.android.synthetic.main.activity_quiz.*
import toothpick.ktp.KTP
import javax.inject.Inject

class QuizActivity : Activity(), NextListener {
    private var chapter_id: Int = 9999


    companion object {
        fun getInstance(context: Context?, chapter_id: Int): Intent {
            val intent = Intent(context, QuizActivity::class.java)
            intent.putExtra("chapter_id", chapter_id)
            return intent
        }
    }

    @Inject
    lateinit var exerciseData: ExerciseData

    override fun onCreate(savedInstanceState: Bundle?) {
        KTP.openScope(APP).inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz)
        chapter_id = intent.getIntExtra("chapter_id", 9999)

        exercise_view_pager.isUserInputEnabled = false
        exercise_view_pager.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        quiz_toolbar.setNavigationOnClickListener { onBackPressed() }


        val exercises = exerciseData.exercise_list.filter { e -> e.chapter_id == chapter_id }
        if (exercises.isEmpty()) {
            no_quiz_info.visibility = View.VISIBLE
        }else{
            no_quiz_info.visibility = View.GONE
            exercise_view_pager.adapter =
                QuizAdapter(exercises, this)
        }



//        exercise_btn_next.setOnClickListener {
//            exercise_view_pager.currentItem = exercise_view_pager.currentItem + 1
//        }
//        exercise_btn_previous.setOnClickListener {
//            exercise_view_pager.currentItem = exercise_view_pager.currentItem - 1
//        }



    }

    override fun onNext() {
        exercise_view_pager.currentItem = exercise_view_pager.currentItem + 1
    }

    override fun onFinish() {
        finish()
    }
}