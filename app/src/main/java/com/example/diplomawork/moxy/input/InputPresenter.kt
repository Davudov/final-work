package com.example.diplomawork.moxy.input

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.diplomawork.di.APP
import com.example.diplomawork.di.relay.InputMsgRelay
import com.example.diplomawork.ui.compiler.input.InputView
import toothpick.ktp.KTP
import javax.inject.Inject

@InjectViewState
class InputPresenter : MvpPresenter<InputView>() {
    @Inject
    lateinit var inputMsgRelay: InputMsgRelay

    override fun onFirstViewAttach() {
        KTP.openScope(APP).inject(this)
        super.onFirstViewAttach()

    }

    fun compile(hashMap: HashMap<String,String>) {
        inputMsgRelay.send(hashMap)
    }

}