package com.example.diplomawork.ui.tutorials

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.diplomawork.BaseFragment
import com.example.diplomawork.R
import com.example.diplomawork.adapters.ChaptersAdapter
import com.example.diplomawork.di.APP
import com.example.diplomawork.entity.ChapterData
import com.example.diplomawork.ui.contents.ContentsActivity
import com.example.diplomawork.listeners.OnClickListener
import kotlinx.android.synthetic.main.fragment_tutorials.*
import toothpick.ktp.KTP
import javax.inject.Inject

class TutorialsFragment : BaseFragment(),
    OnClickListener {
    @Inject
    lateinit var chapterData: ChapterData

    override fun getLayoutId(): Int = R.layout.fragment_tutorials

    override fun onCreate(savedInstanceState: Bundle?) {
        KTP.openScope(APP).inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun viewCreated(savedInstanceState: Bundle?) {
        recycler_chapters.layoutManager = LinearLayoutManager(context)
        recycler_chapters.adapter = ChaptersAdapter(chapterData.chapters_list, this)
    }

    override fun onClick(view: View, position: Int) =
        startActivity(
            ContentsActivity.getInstance(
                context,
                chapterData.chapters_list[position].chapter_id,
                chapterData.chapters_list[position].chapter_name
            )
        )

}