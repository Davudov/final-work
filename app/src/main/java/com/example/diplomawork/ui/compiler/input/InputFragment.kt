package com.example.diplomawork.ui.compiler.input

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.viewpager2.widget.ViewPager2
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.example.diplomawork.BaseFragment
import com.example.diplomawork.R
import com.example.diplomawork.listeners.AccessoryClickListener
import com.example.diplomawork.moxy.input.InputPresenter
import kotlinx.android.synthetic.main.fragment_code_input.*


class InputFragment : BaseFragment(), InputView,
    AccessoryClickListener {
    lateinit var alertDialog: Dialog

    val hashMap: HashMap<String,String> = HashMap()

    @ProvidePresenter
    fun providePresenter(): InputPresenter =
        InputPresenter()

    @InjectPresenter
    lateinit var presenter: InputPresenter


    override fun getLayoutId(): Int = R.layout.fragment_code_input

    override fun viewCreated(savedInstanceState: Bundle?) {
        setAlertDialog()
        accessoryView.accessoryClickListener = this
        compile_btn.setOnClickListener {
            showAlertUserInput()
        }

    }

    private fun setAlertDialog() {
        alertDialog = Dialog(context!!)
        alertDialog.setContentView(R.layout.code_input_alert)
        alertDialog.findViewById<Button>(R.id.run).setOnClickListener {
            onPositiveBtnClicked(
                alertDialog.findViewById<EditText>(R.id.input_editText).text.toString()
            )
        }
        alertDialog.findViewById<Button>(R.id.cancel).setOnClickListener { alertDialog.cancel() }
    }

    private fun onPositiveBtnClicked(input: String) {
        activity?.findViewById<ViewPager2>(R.id.compiler_viewpager)?.setCurrentItem(1, true)
        alertDialog.cancel()
        hashMap["code"] = codeHighlighterEditText.text.toString()
        hashMap["input"] = input
        presenter.compile(hashMap)
    }


    private fun showAlertUserInput() {
        alertDialog.show()
    }


    override fun onClick(str: String, view: View) {
        when (str) {
            "{" -> {
                codeHighlighterEditText.text?.insert(codeHighlighterEditText.selectionStart, str)
                codeHighlighterEditText.text?.insert(
                    codeHighlighterEditText.selectionStart,
                    "\n \t"
                )
            }
            "TAB" -> codeHighlighterEditText.text?.insert(
                codeHighlighterEditText.selectionStart,
                "\t"
            )
            else -> codeHighlighterEditText.text?.insert(
                codeHighlighterEditText.selectionStart,
                str
            )
        }
    }
}


