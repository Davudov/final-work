package com.example.diplomawork.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.diplomawork.BaseFragment

class FragmentStateAdapter(fm: FragmentManager, lifecycle: Lifecycle, private val fragments: ArrayList<BaseFragment>) :
    FragmentStateAdapter(fm,lifecycle) {
    override fun getItemCount(): Int = fragments.size
    override fun createFragment(position: Int): Fragment = fragments[position]
}