package com.example.diplomawork.entity

class
ResponseRun(
    val status: String,
    val error: String,
    val stdout: String,
    val stderr: String,
    val exitcode: String
)