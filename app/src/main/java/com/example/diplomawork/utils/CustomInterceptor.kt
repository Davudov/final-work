package com.example.diplomawork.utils

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import timber.log.Timber

class CustomInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request()
        val builder: Request.Builder = request.newBuilder()
        Timber.d("Requested url: %s", request.url().toString())
//        builder.addHeader("Checkus", "${'$'}2y${'$'}10${'$'}heJNZP6TbdT.DmpZlHp87u1NY7RqdXXV5Ht/rBbvjSsEIqgi42/ku")
        builder.addHeader("x-api-key","YHsbsYV8AENTs3dQWQb2yutVNtpd67SaZI9GX8FOY")
        val response: Response = chain.proceed(builder.build())
        return response
    }
}