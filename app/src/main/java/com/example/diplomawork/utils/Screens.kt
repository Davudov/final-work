package com.example.diplomawork.utils

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.diplomawork.ui.contents.contents.ContentsFragment
import com.example.diplomawork.ui.contents.content_details.ContentDetailsFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object Screens {
    class ContentsScreen : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return ContentsFragment()
        }
    }

    class ContentDetailsScreen(val content_id: Int) : SupportAppScreen() {
        private fun getStaticInstance(): Fragment {
            val fragment = ContentDetailsFragment()
            val args = Bundle()
            args.putInt("content_id", content_id)
            return fragment.apply { arguments = args }
        }

        override fun getFragment(): Fragment {
            return getStaticInstance()
        }
    }
}