package com.example.diplomawork.custom

import android.content.Context
import android.graphics.*
import android.text.Editable
import android.text.Spanned
import android.text.TextWatcher
import android.text.style.CharacterStyle
import android.text.style.ForegroundColorSpan
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import com.example.diplomawork.R
import java.util.regex.Matcher
import java.util.regex.Pattern


@Suppress("UNCHECKED_CAST")
class CodeHighlighterEditText(context: Context, attrs: AttributeSet) :
    AppCompatEditText(context, attrs) {
    var rect = Rect()
    private val paint = Paint()
    private val stringKeywords: String =
        "\\b(package|transient|strictfp|void|char|short|int|long|double|float|const|static|volatile|byte|System|boolean|println|class|interface|native|private|protected|public|final|abstract|synchronized|enum|instanceof|assert|if|else|switch|case|default|break|goto|return|for|while|do|continue|new|throw|throws|try|catch|finally|this|super|extends|implements|import|true|false|null)\\b"
    var patternKeywords: Pattern = Pattern.compile(stringKeywords)

    private val stringRegex: String = "\"([^\"]*)\""
    var patternStrings: Pattern = Pattern.compile(stringRegex)

    private val slashRegex: String = "//.*|(?s)/\\*.*?\\*/"
    var patternSlash: Pattern = Pattern.compile(slashRegex)

    private val numbersRegex: String = "\\d+"
    val patternNumbers: Pattern = Pattern.compile(numbersRegex)

    private val specialDeclarationRegex: String = "(void|class)\\s*(\\w+)\\s*([{(])"
    val declarationPattern: Pattern = Pattern.compile(specialDeclarationRegex)

    private val schemes: Array<ColorScheme> = arrayOf(
        ColorScheme(
            patternKeywords,
            ContextCompat.getColor(context, R.color.syntax_keyword_color)
        ),
        ColorScheme(
            patternStrings,
            ContextCompat.getColor(context, R.color.syntax_strings_color)
        ),
        ColorScheme(
            patternSlash,
            ContextCompat.getColor(context, R.color.syntax_comments_color)
        ),
        ColorScheme(
            patternNumbers,
            ContextCompat.getColor(context, R.color.syntax_numbers_color)
        )
    )

    private val watcher: TextWatcher = object : TextWatcher {

        override fun afterTextChanged(p0: Editable?) {
            filter(p0!!)
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }
    }


    init {
        addTextChangedListener(watcher)
        setText(context.getString(R.string.java_editor_text))
        paint.style = Paint.Style.FILL_AND_STROKE
        paint.isAntiAlias = true
        paint.color = Color.BLACK
        paint.typeface = Typeface.createFromAsset(context.assets, "fonts/muli.ttf")
        val spSize = 10
        val scaledSizeInPixels =
            spSize * resources.displayMetrics.scaledDensity
        paint.textSize = scaledSizeInPixels
    }

    override fun onDraw(canvas: Canvas?) {
        var baseline = baseline
        for (i in 0 until lineCount) {
            canvas?.drawText("${i + 1}", rect.left.toFloat(), baseline.toFloat(), paint)
            baseline += lineHeight
        }
        super.onDraw(canvas)
    }


    fun filter(text: Editable) {
        removeSpans(text, ForegroundColorSpan::class.java)
        schemes.forEach {
            val matcher: Matcher = it.pattern.matcher(text)
            while (matcher.find()) {
                text.setSpan(
                    ForegroundColorSpan(it.color),
                    matcher.start(),
                    matcher.end(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                )
            }
        }

        val matcherDeclaration: Matcher = declarationPattern.matcher(text)
        while (matcherDeclaration.find()) {
            text.setSpan(
                ForegroundColorSpan(
                    ContextCompat.getColor(
                        context,
                        R.color.syntax_special_names_color
                    )
                ),
                matcherDeclaration.start(2),
                matcherDeclaration.end(2),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
    }


    private fun removeSpans(e: Editable, type: Class<out CharacterStyle?>) {
        val spans: Array<CharacterStyle> = e.getSpans(0, e.length, type) as Array<CharacterStyle>
        for (span in spans) {
            e.removeSpan(span)
        }
    }

    class ColorScheme(val pattern: Pattern, val color: Int)
}
