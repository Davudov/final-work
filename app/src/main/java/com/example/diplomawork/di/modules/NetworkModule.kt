package com.example.diplomawork.di.modules

import com.example.diplomawork.di.providers.NetworkProvider
import com.example.diplomawork.di.services.CodeService
import toothpick.config.Module

class NetworkModule :Module() {
    init {
        bind(CodeService::class.java).toProviderInstance(NetworkProvider(CodeService::class.java,"https://api.runmycode.online/")) //TODO
    }
}