package com.example.diplomawork.entity

class Content(
    val chapter_id: Int,
    val chapter_name: String,
    val content_id: Int,
    val content_name: String,
    val content_text: String
)

class ContentData(val content_list: List<Content>)