package com.example.diplomawork.adapters


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.diplomawork.R
import com.example.diplomawork.entity.Content
import com.example.diplomawork.listeners.OnClickListener
import kotlinx.android.synthetic.main.contents_item.view.*

class ContentsAdapter(private val list: List<Content>, private val onClickListener: OnClickListener) :
    RecyclerView.Adapter<ContentsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(
        LayoutInflater.from(
            parent.context
        ).inflate(
            R.layout.contents_item, parent, false
        ),onClickListener
    )

    override fun getItemCount(): Int = list.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position].content_name)
    }

    class ViewHolder(
        itemView: View,
        private val onClickListener: OnClickListener
    ) : RecyclerView.ViewHolder(itemView),View.OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        fun bind(content: String) {
            itemView.content_name.text = content
        }

        override fun onClick(v: View?) {
            onClickListener.onClick(v!!,absoluteAdapterPosition)
        }
    }

}