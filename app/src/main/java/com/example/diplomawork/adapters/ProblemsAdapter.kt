package com.example.diplomawork.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.diplomawork.R
import com.example.diplomawork.entity.ProblemHeader
import com.example.diplomawork.listeners.OnClickListener
import kotlinx.android.synthetic.main.item_problem.view.*

class ProblemsAdapter(
    private val list: List<ProblemHeader>,
    private val onClickListener: OnClickListener
) : RecyclerView.Adapter<ProblemsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(
        LayoutInflater.from(
            parent.context
        ).inflate(
            R.layout.item_problem, parent, false
        ), onClickListener
    )

    override fun getItemCount(): Int = list.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position])
    }

    class ViewHolder(
        itemView: View,
        private val onClickListener: OnClickListener
    ) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        fun bind(problemHeader: ProblemHeader) {
//            if (problemHeader.problem_solved) {
//                itemView.solve_btn.text = "Solved"
//                itemView.solve_btn.setCompoundDrawables(null,null,itemView.context.getDrawable(R.drawable.ic_check_black_24dp),null)
//            } else itemView.solve_btn.text = "Solve"
//
//            if (problemHeader.problem_id == firstUnresolvedId) {
//                itemView.view_holder.elevation = 15f
//                itemView.solve_btn.elevation = 25f
//                itemView.solve_btn.setBackgroundColor(
//                    ContextCompat.getColor(
//                        itemView.context,
//                        R.color.solve_btn_color
//                    )
//                )
//                itemView.solve_btn.setTextColor(
//                    ContextCompat.getColor(
//                        itemView.context,
//                        android.R.color.white
//                    )
//                )
//            }
            itemView.solve_btn.setOnClickListener(this)
            itemView.problem_name.text = problemHeader.problem_name
            itemView.problem_description.text = problemHeader.problem_description
            setLevelColor(problemHeader.problem_level, itemView.problem_level)
        }

        private fun setLevelColor(level: Int, textView: TextView) = when (level) {
            1 -> {
                textView.setTextColor(
                    ContextCompat.getColor(
                        itemView.context,
                        R.color.level_easy_color
                    )
                )
                textView.setText(itemView.context.getString(R.string.problem_level_easy))
            }

            2 -> {
                textView.setTextColor(
                    ContextCompat.getColor(
                        itemView.context,
                        R.color.level_medium_color
                    )
                )
                textView.setText(itemView.context.getString(R.string.problem_level_medium))
            }
            3 -> {
                textView.setTextColor(
                    ContextCompat.getColor(
                        itemView.context,
                        R.color.level_hard_color
                    )
                )
                textView.setText(itemView.context.getString(R.string.problem_level_hard))
            }
            else -> textView.setTextColor(Color.GRAY)
        }

        override fun onClick(v: View?) {
            onClickListener.onClick(v!!, absoluteAdapterPosition)
        }
    }

}
