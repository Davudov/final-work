package com.example.diplomawork.ui.description

import android.os.Bundle
import com.example.diplomawork.BaseFragment
import com.example.diplomawork.R
import kotlinx.android.synthetic.main.fragment_description.*

class DescriptionFragment :BaseFragment() {
    override fun getLayoutId(): Int = R.layout.fragment_description

    override fun viewCreated(savedInstanceState: Bundle?) {
        description.loadUrl("file:///android_asset/data/description.html")
    }
}