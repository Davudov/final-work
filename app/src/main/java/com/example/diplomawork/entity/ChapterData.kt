package com.example.diplomawork.entity

class Chapter(val chapter_id: Int, val chapter_name: String)
class ChapterData(val chapters_list: List<Chapter>)