package com.example.diplomawork.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.diplomawork.R
import com.example.diplomawork.entity.Chapter
import com.example.diplomawork.listeners.OnClickListener
import kotlinx.android.synthetic.main.chapters_item.view.*

class ChaptersAdapter(private val list: List<Chapter>, private val onClickListener: OnClickListener) :
    RecyclerView.Adapter<ChaptersAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(
        LayoutInflater.from(
            parent.context
        ).inflate(
            R.layout.chapters_item, parent, false
        ),onClickListener
    )

    override fun getItemCount(): Int = list.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position].chapter_name)
    }

    class ViewHolder(
        itemView: View,
        private val onClickListener: OnClickListener
    ) : RecyclerView.ViewHolder(itemView),View.OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        fun bind(chapter: String) {
            itemView.chapter_item_text.text = chapter
        }

        override fun onClick(v: View?) {
            onClickListener.onClick(v!!,absoluteAdapterPosition)
        }
    }

}