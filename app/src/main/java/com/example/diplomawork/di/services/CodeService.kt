package com.example.diplomawork.di.services

import com.example.diplomawork.entity.ResponseRun
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface CodeService {
    @POST("run/{lang}")
    fun compileWithInput(
        @Path("lang") lang: String,
        @Query("platform") platform: String,
        @Query("args") input: String,
        @Body code: String
    ): Single<ResponseRun>

    @POST("run/{lang}")
    fun compile(
        @Path("lang") lang: String,
        @Body code: String
    ): Single<ResponseRun>
}