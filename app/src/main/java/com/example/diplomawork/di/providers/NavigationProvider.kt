package com.example.diplomawork.di.providers

import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import javax.inject.Provider

class NavigationProvider : Provider<Router> {

    companion object {
        var cicerone: Cicerone<Router> = Cicerone.create()
        fun getNavigationHolder(): NavigatorHolder = cicerone.navigatorHolder
    }

    override fun get(): Router {
        return cicerone.router
    }

}