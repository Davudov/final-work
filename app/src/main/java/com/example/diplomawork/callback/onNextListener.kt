package com.example.diplomawork.callback

interface NextListener {
    fun onNext()
    fun onFinish()
}