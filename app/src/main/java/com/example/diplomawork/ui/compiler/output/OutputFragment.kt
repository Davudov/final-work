package com.example.diplomawork.ui.compiler.output

import android.os.Bundle
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.example.diplomawork.BaseFragment
import com.example.diplomawork.R
import com.example.diplomawork.di.APP
import com.example.diplomawork.di.relay.InputMsgRelay
import com.example.diplomawork.di.services.CodeService
import com.example.diplomawork.moxy.output.OutputPresenter
import kotlinx.android.synthetic.main.fragment_code_output.*
import toothpick.ktp.KTP
import javax.inject.Inject

class OutputFragment : BaseFragment(), OutputView {
    @Inject
    lateinit var service: CodeService

    @Inject
    lateinit var inputMsgRelay: InputMsgRelay

    @ProvidePresenter
    fun provideOutputPresenter() = OutputPresenter(service, inputMsgRelay)

    @InjectPresenter
    lateinit var outputPresenter: OutputPresenter

    override fun getLayoutId(): Int = R.layout.fragment_code_output

    override fun onCreate(savedInstanceState: Bundle?) {
        KTP.openScope(APP).inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun viewCreated(savedInstanceState: Bundle?) {
        result_info.visibility = View.VISIBLE
    }

    override fun onResponse(stdOut: String) {
        output_textView.text = stdOut
}

    override fun onGetInput(inputMap: HashMap<String,String>) {
        if (inputMap["input"]!!.isNotEmpty()) outputPresenter.compileWithInput(inputMap)
        else outputPresenter.compile(inputMap)
    }

    override fun onError(error: String) {
        output_textView.text = getString(R.string.compilation_failed).plus(error)
    }

    override fun startProgress() {
        result_info.visibility = View.GONE
        output_textView.text = ""
        progress_output.visibility = View.VISIBLE
    }

    override fun stopProgress() {
        progress_output.visibility = View.GONE
    }
}