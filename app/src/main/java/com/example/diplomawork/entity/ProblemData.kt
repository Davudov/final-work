package com.example.diplomawork.entity

class Problem(
    val problem_id: Int,
    val problem_content: String,
    val problem_input: String,
    val problem_expected_output: String
)

class ProblemData(val problem_list: List<Problem>)