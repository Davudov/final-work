package com.example.diplomawork.ui.compiler

import android.os.Bundle
import com.arellomobile.mvp.MvpAppCompatActivity
import com.example.diplomawork.BaseFragment
import com.example.diplomawork.R
import com.example.diplomawork.adapters.FragmentStateAdapter
import com.example.diplomawork.ui.compiler.input.InputFragment
import com.example.diplomawork.ui.compiler.output.OutputFragment
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.activity_compiler.*

class CompilerActivity : MvpAppCompatActivity() {
    private lateinit var adapterActivityStateAdapter: FragmentStateAdapter
    private val tabName = arrayOf("Input", "Output")
    private val fragments: ArrayList<BaseFragment> =
        arrayListOf(
            InputFragment(),
            OutputFragment()
        )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_compiler)
        window.setBackgroundDrawableResource(R.drawable.top_panel_back)

        adapterActivityStateAdapter =
            FragmentStateAdapter(supportFragmentManager, lifecycle, fragments)

        compiler_viewpager.adapter = adapterActivityStateAdapter
        compiler_viewpager.offscreenPageLimit = 2
        TabLayoutMediator(compiler_tabLayout, compiler_viewpager) { tab, position ->
            tab.text = tabName[position]
            compiler_viewpager.setCurrentItem(tab.position, true)
        }.attach()

        compiler_toolbar.setNavigationOnClickListener { onBackPressed() }
    }

}
