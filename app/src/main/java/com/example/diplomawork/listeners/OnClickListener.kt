package com.example.diplomawork.listeners

import android.view.View

interface OnClickListener {
    fun onClick(view: View,position:Int)
}