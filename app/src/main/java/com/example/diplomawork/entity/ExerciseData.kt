package com.example.diplomawork.entity

class Exercise(
    val exercise_id: Int,
    val chapter_id: Int,
    val exercise_question: String,
    val choice_a: String,
    val choice_b: String,
    val choice_c: String,
    val choice_d: String,
    val answer: Int,
    val explanation: String
)

class ExerciseData(val exercise_list: List<Exercise>)