package com.example.diplomawork.di.providers

import android.content.Context
import com.google.gson.Gson
import java.io.IOException
import java.io.InputStream
import java.nio.charset.Charset
import javax.inject.Provider


class JsonParserProvider<T>(private val typeParameterClass: Class<T>,private val application: Context,private val gson: Gson,private val fileName: String) : Provider<T> {
    private fun getJsonFromAssets(): String? {
        val jsonString: String
        jsonString = try {
            val inputStream: InputStream = application.assets.open(fileName)
            val size: Int = inputStream.available()
            val buffer = ByteArray(size)
            inputStream.read(buffer)
            inputStream.close()
            String(buffer, Charset.forName("UTF-8"))
        } catch (e: IOException) {
            e.printStackTrace()
            return null
        }
        return jsonString
    }

    private fun getData(): T {
        val jsonFileString: String? = getJsonFromAssets()

        return gson.fromJson(jsonFileString, typeParameterClass)
    }

    override fun get(): T  = getData()

}