package com.example.diplomawork.utils

import android.content.Context
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.example.diplomawork.R

class FadeAnimation(val context: Context) : Animation() {
    fun addView(v: Array<View>) {
//        val set = AnimationSet(true)
//        set.addAnimation(AnimationUtils.loadAnimation(context, R.anim.anim_bottom_to_original))
//        set.addAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_in))
        v.forEach { t ->
            t.startAnimation(AnimationUtils.loadAnimation(context,R.anim.anim_bottom_to_original))
        }
    }
}