package com.example.diplomawork.ui.compiler.output

import com.arellomobile.mvp.MvpView

interface OutputView : MvpView {
    fun onGetInput(inputMap: HashMap<String,String>)
    fun onResponse(stdOut:String)
    fun onError(error:String)
    fun startProgress()
    fun stopProgress()
}