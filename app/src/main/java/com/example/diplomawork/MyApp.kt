package com.example.diplomawork

import android.app.Application
import com.example.diplomawork.di.APP
import com.example.diplomawork.di.modules.AppModule
import com.example.diplomawork.di.modules.NavigationModule
import com.example.diplomawork.di.modules.NetworkModule
import timber.log.Timber
import toothpick.configuration.Configuration.forDevelopment
import toothpick.configuration.Configuration.forProduction
import toothpick.ktp.KTP

class MyApp:Application(){
    override fun onCreate() {
        super.onCreate()
        initToothpick()
        initTimber()
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun initToothpick() {
        val configuration = if (BuildConfig.DEBUG) forDevelopment() else forProduction()
        KTP.setConfiguration(configuration)
        KTP.openScope(APP).installModules(NetworkModule(),AppModule(this),NavigationModule())
    }
}