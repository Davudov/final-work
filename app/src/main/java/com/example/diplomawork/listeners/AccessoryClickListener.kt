package com.example.diplomawork.listeners

import android.view.View

interface AccessoryClickListener {
    fun onClick(str:String,view:View)
}