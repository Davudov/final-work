package com.example.diplomawork.di.modules

import android.content.Context
import com.example.diplomawork.di.providers.JsonParserProvider
import com.example.diplomawork.di.relay.InputMsgRelay
import com.example.diplomawork.entity.*
import com.google.gson.Gson
import toothpick.config.Module

class AppModule(context: Context) : Module() {
    init {
        val gson = Gson()
        bind(Context::class.java).toInstance(context)
        bind(Gson::class.java).toInstance(gson)
        bind(ContentData::class.java).toProviderInstance(JsonParserProvider(ContentData::class.java,context, gson, "data/tutorials/contents.json"))
        bind(ChapterData::class.java).toProviderInstance(JsonParserProvider(ChapterData::class.java,context, gson, "data/tutorials/chapters.json"))
        bind(ProblemHeaderData::class.java).toProviderInstance(JsonParserProvider(ProblemHeaderData::class.java,context, gson, "data/problems/problem_header.json"))
        bind(ProblemData::class.java).toProviderInstance(JsonParserProvider(ProblemData::class.java,context, gson, "data/problems/problem_body.json"))
        bind(ExerciseData::class.java).toProviderInstance(JsonParserProvider(ExerciseData::class.java,context, gson, "data/tutorials/quiz.json"))
        bind(InputMsgRelay::class.java).toInstance(InputMsgRelay())
    }
}