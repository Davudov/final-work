package com.example.diplomawork.di.relay

import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable

class InputMsgRelay {
    private val msgRelay: PublishRelay<HashMap<String,String>> = PublishRelay.create()

    val observable: Observable<HashMap<String,String>> = msgRelay

    fun send(input: HashMap<String,String>) {
        msgRelay.accept(input)
    }
}