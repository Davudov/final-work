package com.example.diplomawork.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.diplomawork.R
import com.example.diplomawork.entity.Content
import kotlinx.android.synthetic.main.content_detail_item.view.*


class ContentDetailsAdapter(private val list: List<Content>) :
    RecyclerView.Adapter<ContentDetailsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(
        LayoutInflater.from(
            parent.context
        ).inflate(
            R.layout.content_detail_item, parent, false
        )
    )

    override fun getItemCount(): Int = list.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position].content_text)
    }

    class ViewHolder(
        itemView: View
    ) : RecyclerView.ViewHolder(itemView) {

        fun bind(content: String) {
//            itemView.content.text = Html.fromHtml(content)
//            itemView.web_view.loadData(content,"text/html; charset=utf-8", "UTF-8")
            //Font must be placed in assets/fonts folder

            //Font must be placed in assets/fonts folder
            val text =
                ("<html>" +
                        "<header>" +
                        " <link rel=\"stylesheet\" type=\"text/css\" href=\"css/chapter_details_design.css\">\n" +
                        "</header>"+
                        "<body>" +
                        content
                        + "</body></html>")

            itemView.web_view.loadDataWithBaseURL(
                "file:///android_asset/",
                text,
                "text/html",
                "utf-8",
                null
            )
        }
    }

}