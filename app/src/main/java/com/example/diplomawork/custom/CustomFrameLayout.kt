package com.example.diplomawork.custom

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import androidx.annotation.AttrRes
import androidx.annotation.StyleRes

class CustomFrameLayout (context:Context, attrs: AttributeSet,
                         @AttrRes defStyleAttr: Int , @StyleRes defStyleRes: Int ) : FrameLayout(context,attrs, defStyleAttr, defStyleRes) {
    override fun getX(): Float {
        return getX() / getWidth();
    }

    override fun setX(x: Float) {
        val width:Int = width
        if (width > 0) setX(x * width) else -9999
    }
}