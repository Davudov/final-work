package com.example.diplomawork.ui.contents

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import com.example.diplomawork.R
import com.example.diplomawork.di.APP
import com.example.diplomawork.entity.ChapterData
import com.example.diplomawork.utils.Screens
import kotlinx.android.synthetic.main.activity_content_holder.*
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command
import toothpick.ktp.KTP
import javax.inject.Inject


class ContentsActivity : FragmentActivity() {

    companion object {
        fun getInstance(context: Context?, position: Int, chapterName: String): Intent {
            val intent = Intent(context, ContentsActivity::class.java)
            intent.putExtra("chapter_id", position)
            intent.putExtra("chapter_name", chapterName)
            return intent
        }
    }

    @Inject
    lateinit var chapterData: ChapterData

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    @Inject
    lateinit var router: Router

    private val navigator: Navigator = object : SupportAppNavigator(this, R.id.contents_container) {
        override fun setupFragmentTransaction(
            command: Command?,
            currentFragment: Fragment?,
            nextFragment: Fragment?,
            fragmentTransaction: FragmentTransaction?
        ) {
//            fragmentTransaction?.setCustomAnimations(R.animator.fade_in_animator,R.animator.fade_out_animator)
            super.setupFragmentTransaction(
                command,
                currentFragment,
                nextFragment,
                fragmentTransaction
            )
        }
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        KTP.openScope(APP).inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_content_holder)
        window.setBackgroundDrawableResource(R.drawable.top_panel_back)
        router.replaceScreen(
            Screens.ContentsScreen()
        )
        contents_toolbar.setNavigationOnClickListener { onBackPressed() }

    }
}