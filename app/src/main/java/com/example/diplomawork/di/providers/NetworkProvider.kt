package com.example.diplomawork.di.providers

import com.example.diplomawork.utils.CustomInterceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import javax.inject.Provider


class NetworkProvider<T>(private val typeParameterClass: Class<T>, private val baseUrl: String) :
    Provider<T> {

    private fun getClient(): OkHttpClient {
        val client = OkHttpClient.Builder()
//            .connectTimeout(40, TimeUnit.SECONDS) // connect timeout
//            .writeTimeout(40, TimeUnit.SECONDS) // write timeout
//            .readTimeout(40, TimeUnit.SECONDS) // read timeout
        client.addInterceptor(CustomInterceptor())
        return client.build()
    }

    private fun getRetrofitServer(): T {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(getClient())
            .build()
            .create(typeParameterClass)
    }

    override fun get(): T {
        return getRetrofitServer()
    }
}