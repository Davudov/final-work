package com.example.diplomawork.di.modules

import com.example.diplomawork.di.providers.NavigationProvider
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import toothpick.config.Module

class NavigationModule : Module() {
    init {
        bind(Router::class.java).toProviderInstance(NavigationProvider())
        bind(NavigatorHolder::class.java).toInstance(NavigationProvider.getNavigationHolder())
    }
}