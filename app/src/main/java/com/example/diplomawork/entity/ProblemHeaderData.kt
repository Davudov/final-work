package com.example.diplomawork.entity


class ProblemHeader(
    val problem_id: Int,
    val problem_name: String,
    val problem_level: Int,
    val problem_description: String
)

class ProblemHeaderData(val problem_header_list: List<ProblemHeader>)