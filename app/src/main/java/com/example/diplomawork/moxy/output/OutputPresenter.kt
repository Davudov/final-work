package com.example.diplomawork.moxy.output

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.diplomawork.di.relay.InputMsgRelay
import com.example.diplomawork.di.services.CodeService
import com.example.diplomawork.entity.ResponseRun
import com.example.diplomawork.ui.compiler.output.OutputView
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import retrofit2.HttpException

@InjectViewState
class OutputPresenter(
    val service: CodeService,
    val inputMsgRelay: InputMsgRelay
) : MvpPresenter<OutputView>() {
    private val disposable: CompositeDisposable = CompositeDisposable()
    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        listenInputMsgRelay()
    }

    private fun listenInputMsgRelay() {
        disposable.add(
            inputMsgRelay.observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    viewState.onGetInput(it)
                }, { e -> viewState.onError(e.message.toString()) })
        )
    }

    fun compile(inputMap: HashMap<String, String>) {
        disposable.add(service.compile("java", inputMap["code"]!!)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { viewState.startProgress() }
            .doAfterTerminate { viewState.stopProgress() }
            .subscribe({ t ->
                viewState.onResponse(t.stdout)
            }, { e ->
                viewState.onError(getErrorBody(e).stderr)
            })
        )
    }

    fun compileWithInput(inputMap: HashMap<String, String>) {
        disposable.add(service.compileWithInput("java","codesheet", inputMap["input"]!!,inputMap["code"]!!)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { viewState.startProgress() }
            .doAfterTerminate { viewState.stopProgress() }
            .subscribe({ t ->
                viewState.onResponse(t.stdout)
            }, { e ->
                viewState.onError(getErrorBody(e).stderr)
            })
        )
    }

    private fun getErrorBody(e: Throwable): ResponseRun {
        if (e is HttpException) {
            val body: ResponseBody? = e.response().errorBody()
            return Gson().fromJson(body?.string(), ResponseRun::class.java)

        }
        return ResponseRun("error", "error", "error", e.message.toString(), "error")
    }
}