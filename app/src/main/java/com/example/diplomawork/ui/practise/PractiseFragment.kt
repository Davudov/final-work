package com.example.diplomawork.ui.practise

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.diplomawork.BaseFragment
import com.example.diplomawork.R
import com.example.diplomawork.adapters.ProblemsAdapter
import com.example.diplomawork.di.APP
import com.example.diplomawork.entity.ProblemHeaderData
import com.example.diplomawork.listeners.OnClickListener
import com.example.diplomawork.ui.problem.ProblemActivity
import kotlinx.android.synthetic.main.fragment_practise.*
import toothpick.ktp.KTP
import javax.inject.Inject

class PractiseFragment : BaseFragment(),
    OnClickListener {
    @Inject
    lateinit var problemHeaderData: ProblemHeaderData


    override fun onCreate(savedInstanceState: Bundle?) {
        KTP.openScope(APP).inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun getLayoutId(): Int = R.layout.fragment_practise

    override fun viewCreated(savedInstanceState: Bundle?) {
        val list = problemHeaderData.problem_header_list
        problems_recycler.layoutManager = LinearLayoutManager(context)
//        problems_recycler.adapter = ProblemsAdapter(
//            problemHeaderData.problem_header_list, this, list.first { !it.problem_solved }.problem_id)

        problems_recycler.adapter = ProblemsAdapter(
            problemHeaderData.problem_header_list,
            this
        )
    }

    override fun onClick(view: View, position: Int) {
        startActivity(
            ProblemActivity.getInstance(
                context,
                position
            )
        )
    }
}