package com.example.diplomawork

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import com.arellomobile.mvp.MvpAppCompatFragment

abstract class BaseFragment : MvpAppCompatFragment() {
    @LayoutRes()
    abstract fun getLayoutId(): Int

    protected abstract fun viewCreated(savedInstanceState: Bundle?)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(getLayoutId(), container, false)
    }
//
//    override fun onDestroyView() {
//        super.onDestroyView()
//    }
//

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewCreated(savedInstanceState)
    }


}