package com.example.diplomawork.listeners

import android.view.View
import com.example.diplomawork.custom.AccessoryView

class AccessoryViewListener(private val accessoryView: AccessoryView, private val str:String) : View.OnClickListener{
    override fun onClick(v: View?) {
        accessoryView.onClick(str,v!!)
    }
}