package com.example.diplomawork.ui.contents.content_details

import android.os.Bundle
import android.view.View
import androidx.viewpager2.widget.ViewPager2
import com.example.diplomawork.BaseFragment
import com.example.diplomawork.R
import com.example.diplomawork.adapters.ContentDetailsAdapter
import com.example.diplomawork.di.APP
import com.example.diplomawork.entity.ChapterData
import com.example.diplomawork.entity.ContentData
import com.example.diplomawork.utils.FadeAnimation
import kotlinx.android.synthetic.main.activity_content_details.*
import toothpick.ktp.KTP
import javax.inject.Inject

class ContentDetailsFragment : BaseFragment() {
    private var content_id: Int = 9999

    @Inject
    lateinit var contentDetails: ContentData

    @Inject
    lateinit var chapterData: ChapterData

    override fun onCreate(savedInstanceState: Bundle?) {
        KTP.openScope(APP).inject(this)
        super.onCreate(savedInstanceState)
        content_id = arguments!!.getInt("content_id", 9999)
    }

    override fun getLayoutId(): Int = R.layout.activity_content_details

    override fun viewCreated(savedInstanceState: Bundle?) {
        val contentsToolbar: androidx.appcompat.widget.Toolbar =
            activity!!.findViewById(R.id.contents_toolbar)
        contentsToolbar.title =
            contentDetails.content_list[content_id - 1].chapter_name

        val animUtil = FadeAnimation(context!!)
        animUtil.addView(
            arrayOf(
                view_pager,content_details_btn_previous,content_details_btn_next
            )
        )

        view_pager.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        view_pager.adapter = ContentDetailsAdapter(contentDetails.content_list)
        view_pager.setCurrentItem(content_id - 1, false)


        view_pager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {

                contentsToolbar.title =
                    contentDetails.content_list[position].chapter_name
                content_details_btn_previous.visibility =
                    if (position == 0) View.GONE else View.VISIBLE
                content_details_btn_next.visibility =
                    if (position == contentDetails.content_list.size - 1) View.GONE else View.VISIBLE
            }
        })

        content_details_btn_next.setOnClickListener {
            view_pager.currentItem = view_pager.currentItem + 1
        }
        content_details_btn_previous.setOnClickListener {
            view_pager.currentItem = view_pager.currentItem - 1
        }
    }
}