package com.example.diplomawork.ui.problem

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.arellomobile.mvp.MvpAppCompatActivity
import com.example.diplomawork.R
import com.example.diplomawork.di.APP
import com.example.diplomawork.entity.ProblemData
import com.example.diplomawork.entity.ProblemHeaderData
import com.example.diplomawork.ui.compiler.CompilerActivity
import com.r0adkll.slidr.Slidr
import kotlinx.android.synthetic.main.activity_problem.*
import toothpick.ktp.KTP
import javax.inject.Inject

class ProblemActivity :MvpAppCompatActivity(), View.OnClickListener {
    @Inject
    lateinit var problemData: ProblemData
    @Inject
    lateinit var problemHeaderData: ProblemHeaderData

    var position: Int = 999
    companion object {
        fun getInstance(context: Context?, position: Int): Intent {
            val intent = Intent(context, ProblemActivity::class.java)
            intent.putExtra("position", position)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        KTP.openScope(APP).inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_problem)
        Slidr.attach(this)
        position = intent.getIntExtra("position",999)
        content.text = problemData.problem_list[position].problem_content
        input.text = problemData.problem_list[position].problem_input
        problem_sample_output.text = problemData.problem_list[position].problem_expected_output
        activity_problem_toolbar.title = problemHeaderData.problem_header_list[position].problem_name
        activity_problem_toolbar.setNavigationOnClickListener { onBackPressed() }
        go_to_editor_btn.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        startActivity(Intent(this,CompilerActivity::class.java))
    }
}