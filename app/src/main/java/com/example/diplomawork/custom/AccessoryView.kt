package com.example.diplomawork.custom

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import com.example.diplomawork.R
import com.example.diplomawork.listeners.AccessoryClickListener
import com.example.diplomawork.listeners.AccessoryViewListener


class AccessoryView(context: Context, attr: AttributeSet) : LinearLayout(context, attr) {
    var accessoryClickListener: AccessoryClickListener? = null


    init {
        set()
    }

    fun set() {
        orientation = LinearLayout.HORIZONTAL
        removeAllViews()
        val strArr = arrayOf(
            "TAB",
            "{",
            "}",
            "(",
            ")",
            "\"",
            ".",
            ";",
            "/",
            "<",
            ">",
            "!",
            "=",
            "[",
            "]",
            "&",
            "|",
            "#",
            "*",
            "+",
            "-",
            ":",
            "%",
            ",",
            "_",
            "@",
            "?",
            "^",
            "'"
        )
        for (i in 0 until 29) {
            val str: String = strArr[i]
            val i2 = ((resources.displayMetrics.densityDpi) * 50) / 160
            val button = Button(context)
            button.setLayoutParams(LayoutParams(i2, i2))
            button.setGravity(17)
            button.setText(str)
            button.setTextColor(Color.WHITE)
            button.setTextSize(15.0f)
            button.setAllCaps(true)
            button.setTextColor(ContextCompat.getColor(context, R.color.colorTextNormalDN))
            button.setClickable(true)
            button.setOnClickListener(
                AccessoryViewListener(
                    this,
                    str
                )
            )
//            button.setBackgroundResource(this.b.resourceId)
            button.setBackgroundColor(ContextCompat.getColor(context, R.color.gray_light))
            addView(button)
        }
    }

    fun onClick(string: String, view: View) {
        accessoryClickListener?.onClick(string,view)
    }
}